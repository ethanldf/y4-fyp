# Web application portal and python API Server

## Prerequisites
- Python 3 (Anaconda recommended)  
- Node.js  
- Angular CLI 8.3.25  
- Flask (install with 'pip3 install Flask')  
- Nvidia CUDA and CUDNN, recommended for best Machine Learning performance  

## Installing dependencies
cd to .\y4-fyp\src\my-app, and run command 'npm i'  
  
sudo apt -y install python3   
sudo apt update  
sudo apt -y install python3-pip  
sudo apt -y install python-pip  

pip3 install opencv-python  
pip3 install keras   
pip3 install tensorflow==1.15.0  
pip3 install scikits.bootstrap  
pip3 install pydub  
pip3 install matplotlib  
pip3 install mtcnn  
pip3 install pandas  
pip3 install numpy  
pip3 install pymongo  
pip3 install flask-cors --upgrade  
pip3 install --pre Werkzeug==0.16.0  
  
If hosting with simplehttpserver, install 'npm i simplehttpserver'  

## Instructions to run application and server

### localhost

Application and server uses localhost by default  
  
cd to .\y4-fyp\src\my-app, run command 'ng serve'  
cd to .\y4-fyp\src\backend, run command 'python main.py'  
application should now be accessible with address, 'http://localhost:4200'  

### simplehttpserver:

If hosting with simplehttpserver:  
  
Modify .\y4-fyp\src\my-app\src\app\services\patients.service.ts, variable 'private patientUrl' in line 23, to the local ip address, using port 5000  
Modify .\y4-fyp\src\backend\main.py, app.run parameter in line 401, to the local ip address  
cd to .\y4-fyp\src\my-app, run command 'ng build --prod'  
cd to .\y4-fyp\src\my-app\dist\my-app, run command 'simplehttpserver'  
cd to .\y4-fyp\src\backend, run command 'python main.py'  
application should now be accessible with local ip address, port 4200  