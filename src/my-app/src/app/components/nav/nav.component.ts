import { Component } from '@angular/core';
import { BreakpointObserver, Breakpoints } from '@angular/cdk/layout';
import { Observable } from 'rxjs';
import { map, shareReplay } from 'rxjs/operators';

@Component({
  selector: 'app-nav',
  templateUrl: './nav.component.html',
  styleUrls: ['./nav.component.scss']
})
export class NavComponent {
  isHandset$: Observable<boolean> = this.breakpointObserver
    .observe(Breakpoints.Handset)
    .pipe(
      map(result => result.matches),
      shareReplay()
    );

  constructor(private breakpointObserver: BreakpointObserver) {}

  toggle(drawer: any) {
    drawer.toggle();

    this.setInterval(
      // tslint:disable-next-line: only-arrow-functions
      function() {
        window.dispatchEvent(new Event('resize'));
      },
      200,
      5
    );
  }

  setInterval(callback, interval, epoch) {
    let x = 0;
    // tslint:disable-next-line: only-arrow-functions
    const intervalID = window.setInterval(function() {
      callback();

      if (++x === epoch) {
        window.clearInterval(intervalID);
      }
    }, interval);
  }
}
