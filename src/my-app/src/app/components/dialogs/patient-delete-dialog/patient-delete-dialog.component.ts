import { Component, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';

import { Patient } from '../../../services/patients';

export interface DialogData {
  patient: Patient;
  confirm: boolean;
}

@Component({
  // tslint:disable-next-line: component-selector
  selector: 'patient-delete-dialog',
  templateUrl: 'patient-delete-dialog.component.html'
})
// tslint:disable-next-line: component-class-suffix
export class PatientDeleteDialogComponent {
  constructor(
    public dialogRef: MatDialogRef<PatientDeleteDialogComponent>,
    @Inject(MAT_DIALOG_DATA) public data: DialogData
  ) {}

  onNoClick(): void {
    this.data.confirm = false;
    this.dialogRef.close();
  }

  confirmDel(): void {
    this.data.confirm = true;
  }
}
