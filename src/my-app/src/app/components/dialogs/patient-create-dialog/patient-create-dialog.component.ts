import { Component, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';

export interface DialogData {
  message: any;
}

@Component({
  // tslint:disable-next-line: component-selector
  selector: 'patient-create-dialog',
  templateUrl: 'patient-create-dialog.component.html'
})
// tslint:disable-next-line: component-class-suffix
export class PatientCreateDialogComponent {
  constructor(
    public dialogRef: MatDialogRef<PatientCreateDialogComponent>,
    @Inject(MAT_DIALOG_DATA) public data: DialogData
  ) {}

  onNoClick(): void {
    this.dialogRef.close();
  }
}
