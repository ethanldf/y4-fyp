import { NgModule } from '@angular/core';
import { PagesComponent } from './pages.component';
import { PagesRoutingModule } from './pages-routing.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';

import { LayoutModule } from '@angular/cdk/layout';
import { MatToolbarModule } from '@angular/material/toolbar';
import { MatButtonModule } from '@angular/material/button';
import { MatSidenavModule } from '@angular/material/sidenav';
import { MatIconModule } from '@angular/material/icon';
import { MatListModule } from '@angular/material/list';
import { MatGridListModule } from '@angular/material/grid-list';
import { MatCardModule } from '@angular/material/card';
import { MatMenuModule } from '@angular/material/menu';
import { MatTableModule } from '@angular/material/table';
import { MatPaginatorModule } from '@angular/material/paginator';
import { MatSortModule } from '@angular/material/sort';
import { DragDropModule } from '@angular/cdk/drag-drop';
import { MatInputModule } from '@angular/material/input';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatSelectModule } from '@angular/material/select';
import { MatExpansionModule } from '@angular/material/expansion';
import { MatDatepickerModule } from '@angular/material/datepicker';
import { MatNativeDateModule } from '@angular/material/core';
import { MatDialogModule } from '@angular/material/dialog';
import { MatSnackBarModule } from '@angular/material/snack-bar';
import { MatSliderModule } from '@angular/material/slider';
import { MatProgressBarModule } from '@angular/material/progress-bar';
import { MatProgressSpinnerModule } from '@angular/material/progress-spinner';

import { ComponentsModule } from '../components/components.module';
import { PatientDeleteDialogComponent } from '../components/dialogs/patient-delete-dialog/patient-delete-dialog.component';
import { PatientCreateDialogComponent } from '../components/dialogs/patient-create-dialog/patient-create-dialog.component';

import { PatientListComponent } from './patient-list/patient-list.component';
import { PatientRecordsComponent } from './patient-records/patient-records.component';
import { RecordInfoComponent } from './record-info/record-info.component';
import { AddPatientComponent } from './add-patient/add-patient.component';
import { UpdatePatientComponent } from './update-patient/update-patient.component';

import { Ng2SearchPipeModule } from 'ng2-search-filter';

@NgModule({
  imports: [
    PagesRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    LayoutModule,
    MatToolbarModule,
    MatButtonModule,
    MatSidenavModule,
    MatIconModule,
    MatListModule,
    MatGridListModule,
    MatCardModule,
    MatMenuModule,
    MatTableModule,
    MatPaginatorModule,
    MatSortModule,
    ComponentsModule,
    DragDropModule,
    MatInputModule,
    MatExpansionModule,
    MatFormFieldModule,
    MatSelectModule,
    MatDatepickerModule,
    HttpClientModule,
    MatNativeDateModule,
    MatDialogModule,
    MatSnackBarModule,
    MatSliderModule,
    MatProgressBarModule,
    MatProgressSpinnerModule,
    Ng2SearchPipeModule
  ],
  declarations: [
    PagesComponent,
    PatientListComponent,
    PatientRecordsComponent,
    RecordInfoComponent,
    AddPatientComponent,
    UpdatePatientComponent,
    PatientCreateDialogComponent,
    PatientDeleteDialogComponent
  ],
  entryComponents: [PatientCreateDialogComponent, PatientDeleteDialogComponent]
})
export class PagesModule {}
