import { Component, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { MatSnackBar } from '@angular/material/snack-bar';
import { Sort } from '@angular/material/sort';

import { Patient } from '../../services/patients';
import { PatientService } from '../../services/patients.service';
import { ActivatedRoute } from '@angular/router';

import { PatientDeleteDialogComponent } from 'src/app/components/dialogs/patient-delete-dialog/patient-delete-dialog.component';

export interface DialogData {
  patient: Patient;
  confirm: boolean;
}

@Component({
  selector: 'app-patient-list',
  templateUrl: './patient-list.component.html',
  styleUrls: ['./patient-list.component.scss']
})
export class PatientListComponent implements OnInit {
  patients: Patient[] = [];
  confirm: boolean;
  data: DialogData;
  message: any;
  sortedData: Patient[];

  term: string;

  constructor(
    private route: ActivatedRoute,
    private patientService: PatientService,
    public dialog: MatDialog,
    private snackBar: MatSnackBar
  ) {
    this.sortedData = this.patients.slice();
  }

  ngOnInit() {
    this.getPatients();
  }

  getPatients(): void {
    this.patientService
      .getPatients()
      .subscribe(
        patients => (
          (this.patients = patients), (this.sortedData = this.patients.slice())
        )
      );
  }

  delete(patient: Patient, confirm: boolean): void {
    this.patientService.deletePatient(patient, confirm).subscribe(result => {
      this.message = result as JSON;

      this.openSnackBar(this.message.result);
      this.getPatients();
    });
  }

  openDialog(patient: Patient) {
    const dialogRef = this.dialog.open(PatientDeleteDialogComponent, {
      width: '500px',
      data: { patient, confirm: this.confirm }
    });

    dialogRef.afterClosed().subscribe(result => {
      if (result !== undefined) {
        this.data = result;

        this.delete(patient, this.data.confirm);
      }
    });
  }

  openSnackBar(message: string) {
    this.snackBar.open(message, 'Dismiss', {
      duration: 3000
    });
  }

  sortData(sort: Sort) {
    const data = this.patients.slice();
    if (!sort.active || sort.direction === '') {
      this.sortedData = data;
      return;
    }

    this.sortedData = data.sort((a, b) => {
      const isAsc = sort.direction === 'asc';
      switch (sort.active) {
        case 'patientId':
          return compare(a.patientId, b.patientId, isAsc);
        case 'nric':
          return compare(a.nric, b.nric, isAsc);
        case 'firstname':
          return compare(a.firstname, b.firstname, isAsc);
        default:
          return 0;
      }
    });
  }
}

function compare(a: number | string, b: number | string, isAsc: boolean) {
  return (a < b ? -1 : 1) * (isAsc ? 1 : -1);
}
