import { Component, OnInit } from '@angular/core';
import { Location } from '@angular/common';
import { ActivatedRoute, NavigationExtras, Router } from '@angular/router';
import { MatSnackBar } from '@angular/material/snack-bar';
import { Sort } from '@angular/material/sort';

import { Patient } from '../../services/patients';
import { PatientService } from '../../services/patients.service';
import { Record } from '../../services/records';

@Component({
  selector: 'app-patient-records',
  templateUrl: './patient-records.component.html',
  styleUrls: ['./patient-records.component.scss']
})
export class PatientRecordsComponent implements OnInit {
  constructor(
    private patientService: PatientService,
    private location: Location,
    private route: ActivatedRoute,
    private router: Router,
    private snackBar: MatSnackBar
  ) { }
  patient: Patient;
  id: any;
  selectedFile: File;
  message: any;
  recordCount: any;
  sortedData: Record[];

  dl: boolean;
  anal: boolean;
  adding: boolean;
  allowUpload: boolean;

  term: string;

  private setting = {
    element: {
      dynamicDownload: null as HTMLElement
    }
  };

  ngOnInit() {
    this.getPatient();
  }

  getPatient(): void {
    this.id = this.route.snapshot.paramMap.get('id');
    this.patientService.getPatient(this.id).subscribe(patient => {
      this.patient = patient;
      this.sortedData = this.patient.records.slice();

      this.recordCount = this.patient.records.length;
    });
  }

  onFileSelected(event) {
    this.selectedFile = event.target.files[0] as File;
    this.allowUpload = true;
  }

  addRecord(): void {
    this.adding = true;

    const fd = new FormData();

    if (this.selectedFile != null) {
      fd.append('file', this.selectedFile, this.selectedFile.name);

      this.patientService.addRecord(this.id, fd).subscribe(result => {
        this.message = result as JSON;
        this.openMessageSnack(this.message.result);

        this.allowUpload = false;
        this.adding = false;

        this.getPatient();
      });
    } else {
      this.openFileSnack();
    }
  }

  deleteRecord(record: Record): void {
    // tslint:disable-next-line: no-string-literal
    this.patientService.deleteRecord(this.id, record._id['$oid']).subscribe(result => {
      this.message = result as JSON;
      this.openMessageSnack(this.message.result);

      this.getPatient();
    });
  }

  getRecordAnal(record: Record): void {
    this.anal = true;

    this.patientService
      // tslint:disable-next-line: no-string-literal
      .getRecordAnal(this.id, record._id['$oid'])
      .subscribe(result => {
        this.anal = false;
        this.getPatient();
      });
  }

  getRecord(record: Record): void {
    this.dl = true;

    this.patientService
      // tslint:disable-next-line: no-string-literal
      .getRecord(this.id, record._id['$oid'])
      .subscribe(result => {
        this.downloadJson(record.name, result);
      });
  }

  downloadJson(filename, content) {
    const dataStr = 'data:text/json;charset=utf-8,' + encodeURIComponent(JSON.stringify(content));
    const downloadNode = document.createElement('a');

    downloadNode.setAttribute('href', dataStr);
    downloadNode.setAttribute('download', filename + '.json');
    document.body.appendChild(downloadNode); // for firefox
    downloadNode.click();
    downloadNode.remove();

    this.dl = false;
  }

  viewRecord(record: Record) {
    const navigationExtras: NavigationExtras = {
      queryParams: {
        patientId: this.id,
        // tslint:disable-next-line: no-string-literal
        recordId: record._id['$oid']
      }
    };
    this.router.navigate(['/pages/record-info/'], navigationExtras);
  }

  openMessageSnack(message: string) {
    this.snackBar.open(message, 'Dismiss', {
      duration: 3000
    });
  }

  openFileSnack() {
    this.snackBar.open('No File Chosen', 'Dismiss', {
      duration: 3000
    });
  }

  sortData(sort: Sort) {
    const data = this.patient.records.slice();
    if (!sort.active || sort.direction === '') {
      this.sortedData = data;
      return;
    }

    this.sortedData = data.sort((a, b) => {
      const isAsc = sort.direction === 'asc';
      switch (sort.active) {
        case 'added':
          return compare(a.added, b.added, isAsc);
        case 'name':
          return compare(a.name, b.name, isAsc);
        default:
          return 0;
      }
    });
  }
}

function compare(a: number | string, b: number | string, isAsc: boolean) {
  return (a < b ? -1 : 1) * (isAsc ? 1 : -1);
}
