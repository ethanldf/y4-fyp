import { Component, OnInit, HostListener } from '@angular/core';
import { Location } from '@angular/common';
import { ActivatedRoute } from '@angular/router';

import { PatientService } from '../../services/patients.service';
import { RecordData } from '../../services/record_data';

import Plotly from 'plotly.js-dist';

@Component({
  selector: 'app-record-info',
  templateUrl: './record-info.component.html',
  styleUrls: ['./record-info.component.scss']
})
export class RecordInfoComponent implements OnInit {
  @HostListener('window:resize', ['$event'])
sizeChange(event) {
  // console.log('size changed.', event);
}
  // tslint:disable: member-ordering
  recordData: RecordData;
  recordId: any;
  patientId: any;

  maxX: any;
  maxY: any;
  maxZ: any;

  valueX: any;
  valueY: any;
  valueZ: any;

  selectedX = 'X';
  selectedY = 'Y';
  selectedZ = 'Stroke';
  selectedS = 'Pressure';
  selectedC = 'Time';

  isGraphed: boolean;
  visible: boolean;

  constructor(
    private patientService: PatientService,
    private location: Location,
    private route: ActivatedRoute
  ) {
    this.route.queryParams.subscribe(params => {
      this.recordId = params.recordId;
      this.patientId = params.patientId;
    });
  }

  ngOnInit() {
    // this.getRecord();
  }

  getRecord(): void {
    this.patientService
      .getRecord(this.patientId, this.recordId)
      .subscribe(recordData => {
        this.recordData = recordData;

        // tslint:disable: no-eval
        this.maxX = this.findMinMax(eval('this.recordData.' + this.selectedX))[0];
        this.maxY = this.findMinMax(eval('this.recordData.' + this.selectedY))[0];
        this.maxZ = this.findMinMax(eval('this.recordData.' + this.selectedZ))[0];

        // console.log(this.maxX);

        this.plotGraph();
        window.dispatchEvent(new Event('resize'));
        this.visible = true;
      });
  }

  findMinMax(arr) {
    let max = -Number.MAX_VALUE;
    let min = Number.MAX_VALUE;

    console.log(arr);

    // tslint:disable: no-eval
    for (let i = 0; i <= arr.length; i++) {
      if (arr[i] >= max) {
        max = arr[i];
      }
      if (arr[i] <= min) {
        min = arr[i];
      }
    }

    return [max, min];
  }

  plotGraph() {
    const data = [];

    // tslint:disable: no-eval
    // const min = Math.min(...eval('this.recordData.' + this.selectedC));
    // const max = Math.max(...eval('this.recordData.' + this.selectedC));

    const cMaxMin = this.findMinMax(eval('this.recordData.' + this.selectedC));
    const activityMax = this.findMinMax(this.recordData.Activity)[0];

    for (let i = 0; i <= activityMax; i++) {
      const arrayX = [];
      const arrayY = [];
      const arrayZ = [];
      const arrayS = [];
      const arrayC = [];

      // tslint:disable-next-line: prefer-for-of
      for (let j = 0; j <= this.recordData.Activity.length; j++) {
        if (this.recordData.Activity[j] === i) {
          arrayX.push(eval('this.recordData.' + this.selectedX + '[j]'));
          arrayY.push(eval('this.recordData.' + this.selectedY + '[j]'));
          arrayZ.push(eval('this.recordData.' + this.selectedZ + '[j]'));
          arrayS.push(eval('this.recordData.' + this.selectedS + '[j]'));
          arrayC.push(eval('this.recordData.' + this.selectedC + '[j]'));
        }
      }

      data[i] = {
        name: 'Activity ' + i,
        x: arrayX,
        y: arrayY,
        z: arrayZ,
        mode: 'markers',
        marker: {
          size: arrayS,
          color: arrayC,
          cmin: cMaxMin[1],
          cmax: cMaxMin[0],
          colorscale: [
            ['0.0', 'rgb(255,0,255)'],
            ['0.1', 'rgb(128,0,255)'],
            ['0.2', 'rgb(0,0,255)'],
            ['0.3', 'rgb(0,128,255)'],
            ['0.4', 'rgb(0,255,255)'],
            ['0.5', 'rgb(0,255,128)'],
            ['0.6', 'rgb(0,255,0)'],
            ['0.7', 'rgb(128,255,0)'],
            ['0.8', 'rgb(255,255,0)'],
            ['0.9', 'rgb(255,128,0)'],
            ['1.0', 'rgb(255,0,0)']
          ],
          colorbar: {
            title: this.selectedC,
            len: 0.9,
            x: 1
          },
          sizeref: (2.0 * Math.max(...arrayS)) / 15 ** 2,
          sizemode: 'area',
          line: {
            color: 'rgba(0, 0, 0, 0.1)',
            width: 0.01
          },
          opacity: 0.8
        },
        type: 'scatter3d',
        hovertemplate:
          '<b>' +
          this.patientId +
          '</b><br><br>' +
          this.selectedX + ': %{x}<br>' +
          this.selectedY + ': %{y}<br><br>' +
          this.selectedZ + ': %{z}<br>' +
          this.selectedS + ': %{marker.size:,}<br>' +
          this.selectedC + ': %{marker.color:,}' +
          '<extra></extra>'
      };
    }

    const layout = {
      autosize: true,
      height: 700,
      margin: {
        l: 0,
        r: 0,
        b: 0,
        t: 0,
        autoexpand: true
      },
      scene: {
        xaxis: {
          title: this.selectedX,
          backgroundcolor: 'rgb(200, 200, 230)',
          gridcolor: 'rgb(255, 255, 255)',
          showbackground: true,
          zerolinecolor: 'rgb(255, 255, 255)'
        },
        yaxis: {
          title: this.selectedY,
          backgroundcolor: 'rgb(230, 200, 230)',
          gridcolor: 'rgb(255, 255, 255)',
          showbackground: true,
          zerolinecolor: 'rgb(255, 255, 255)'
        },
        zaxis: {
          title: this.selectedZ,
          backgroundcolor: 'rgb(230, 230, 200)',
          gridcolor: 'rgb(255, 255, 255)',
          showbackground: true,
          zerolinecolor: 'rgb(255, 255, 255)'
        },
        camera: {
          eye: { x: 0, y: 0, z: 2 },
          up: { x: 0, y: 1, z: 0 },
          center: { x: 0, y: 0, z: 0 }
        }
      },
      paper_bgcolor: '#F8F8F8',
      showlegend: true,
      legend: {
        orientation: 'h',
        yanchor: 'top',
        xanchor: 'center',
        y: -0.01,
        x: 0.5
      }
    };

    const config = {
      responsive: true,
      displaylogo: false,
      toImageButtonOptions: {
        format: 'svg',
        filename: 'graph_image',
        height: 500,
        width: 1000,
        scale: 1
      }
    };

    Plotly.newPlot('graph', data, layout, config);
  }

  formatLabelX(valueX: number) {
    const xLabel = eval('this.last' + this.selectedX);

    if (valueX >= xLabel) {
      return Math.round(valueX / xLabel) + 'k';
    }
    return valueX;
  }

  formatLabelY(valueY: number) {
    const yLabel = eval('this.last' + this.selectedY);

    if (valueY >= yLabel) {
      return Math.round(valueY / yLabel) + 'k';
    }
    return valueY;
  }

  formatLabelZ(valueZ: number) {
    const zLabel = eval('this.last' + this.selectedZ);

    if (valueZ >= zLabel) {
      return Math.round(valueZ / zLabel) + 'k';
    }
    return valueZ;
  }

  changeGraphX() {
    Plotly.relayout('graph', 'scene.xaxis.range', [0, this.valueX]);
  }

  changeGraphY() {
    Plotly.relayout('graph', 'scene.yaxis.range', [0, this.valueY]);
  }

  changeGraphZ() {
    Plotly.relayout('graph', 'scene.zaxis.range', [0, this.valueZ]);
  }

  defaultInput() {
    this.selectedX = 'X';
    this.selectedY = 'Y';
    this.selectedZ = 'Stroke';
    this.selectedS = 'Pressure';
    this.selectedC = 'Time';
  }

  submitInput() {
    this.isGraphed = true;
    this.getRecord();
  }

  resetInput() {
    Plotly.purge('graph');
    this.visible = false;
    this.isGraphed = false;
  }
}
