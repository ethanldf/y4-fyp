import { Component, OnInit, Input } from '@angular/core';
import { Location } from '@angular/common';
import { FormControl, Validators } from '@angular/forms';
import { MatDialog } from '@angular/material/dialog';

import { Patient } from '../../services/patients';
import { PatientService } from '../../services/patients.service';
import { Record } from '../../services/records';

import { PatientCreateDialogComponent } from 'src/app/components/dialogs/patient-create-dialog/patient-create-dialog.component';

export interface DialogData {
  message: any;
}

@Component({
  selector: 'app-add-patient',
  templateUrl: './add-patient.component.html',
  styleUrls: ['./add-patient.component.scss']
})
export class AddPatientComponent implements OnInit {
  record: Record[];

  @Input() patient: Patient = {
    patientId: '',
    nric: '',
    firstname: '',
    lastname: '',
    gender: '',
    dob: '',
    added: '',
    records: undefined
  };

  genderControl = new FormControl('', [Validators.required]);
  genders = ['Male', 'Female'];

  message: JSON;

  constructor(
    private patientService: PatientService,
    private location: Location,
    public dialog: MatDialog
  ) {}

  ngOnInit() {}

  save(): void {
    this.patient.added = new Date().toISOString();
    this.patientService.addPatient(this.patient).subscribe(result => {
      this.message = result as JSON;

      this.openDialog();
    });
  }

  goBack(): void {
    this.location.back();
  }

  openDialog() {
    const dialogRef = this.dialog.open(PatientCreateDialogComponent, {
      width: '300px',
      data: { message: this.message }
    });
  }
}
