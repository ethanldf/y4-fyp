import { RouterModule, Routes } from '@angular/router';
import { NgModule } from '@angular/core';

import { PagesComponent } from './pages.component';
import { PatientListComponent } from './patient-list/patient-list.component';
import { PatientRecordsComponent } from './patient-records/patient-records.component';
import { RecordInfoComponent } from './record-info/record-info.component';
import { AddPatientComponent } from './add-patient/add-patient.component';
import { UpdatePatientComponent } from './update-patient/update-patient.component';

const routes: Routes = [
  {
    path: '',
    component: PagesComponent,
    children: [
      { path: '', redirectTo: 'patients', pathMatch: 'full' },
      {
        path: 'patients',
        component: PatientListComponent
      },
      {
        path: 'patient-records/:id',
        component: PatientRecordsComponent
      },
      {
        path: 'record-info',
        component: RecordInfoComponent
      },
      {
        path: 'add-patient',
        component: AddPatientComponent
      },
      {
        path: 'update-patient/:id',
        component: UpdatePatientComponent
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class PagesRoutingModule {}
