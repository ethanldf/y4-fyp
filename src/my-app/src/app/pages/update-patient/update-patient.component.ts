import { Component, OnInit, Input } from '@angular/core';
import { Location } from '@angular/common';
import { FormControl, Validators } from '@angular/forms';
import { MatDialog } from '@angular/material/dialog';
import { ActivatedRoute } from '@angular/router';
import { MatSnackBar } from '@angular/material/snack-bar';

import { Patient } from '../../services/patients';
import { PatientService } from '../../services/patients.service';
import { Record } from '../../services/records';

import { PatientCreateDialogComponent } from 'src/app/components/dialogs/patient-create-dialog/patient-create-dialog.component';

export interface DialogData {
  message: any;
}

@Component({
  selector: 'app-update-patient',
  templateUrl: './update-patient.component.html',
  styleUrls: ['./update-patient.component.scss']
})
export class UpdatePatientComponent implements OnInit {
  selectedFile: File;

  @Input() patient: Patient = {
    patientId: '',
    nric: '',
    firstname: '',
    lastname: '',
    gender: '',
    dob: '',
    added: '',
    records: undefined
  };

  genderControl = new FormControl('', [Validators.required]);
  genders = ['Male', 'Female'];

  message: JSON;

  constructor(
    private patientService: PatientService,
    private location: Location,
    public dialog: MatDialog,
    private route: ActivatedRoute
  ) {}

  ngOnInit() {
    this.getPatient();
  }

  getPatient(): void {
    this.patient.patientId = this.route.snapshot.paramMap.get('id');
    this.patientService
      .getPatient(this.patient.patientId)
      .subscribe(patient => {
        this.patient = patient;
      });
  }

  save(): void {
    this.patient.added = new Date().toISOString();
    this.patientService.updatePatient(this.patient).subscribe(result => {
      this.message = result as JSON;

      this.openDialog();
    });
  }

  goBack(): void {
    this.location.back();
  }

  openDialog() {
    const dialogRef = this.dialog.open(PatientCreateDialogComponent, {
      width: '300px',
      data: { message: this.message }
    });
  }
}
