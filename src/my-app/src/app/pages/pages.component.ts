import { Component } from '@angular/core';
import { MENU_ITEMS } from './pages-menu';

@Component({
  selector: 'app-pages',
  templateUrl: './pages.component.html',
  styleUrls: ['pages.component.scss']
})
export class PagesComponent {
  menu = MENU_ITEMS;
  isExpanded = false;

  onClick() {
    this.isExpanded = !this.isExpanded;

    this.setInterval(
      // tslint:disable-next-line: only-arrow-functions
      function() {
        window.dispatchEvent(new Event('resize'));
      },
      200,
      5
    );
  }

  setInterval(callback, interval, epoch) {
    let x = 0;
    // tslint:disable-next-line: only-arrow-functions
    const intervalID = window.setInterval(function() {
      callback();

      if (++x === epoch) {
        window.clearInterval(intervalID);
      }
    }, interval);
  }
}
