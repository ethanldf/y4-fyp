export const MENU_ITEMS: any[] = [
  {
    title: 'Patients',
    icon: 'emoji_people',
    link: '/pages/patients'
  },
  {
    title: 'Add Patient',
    icon: 'add',
    link: '/pages/add-patient'
  }
];
