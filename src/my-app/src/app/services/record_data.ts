export class RecordData {
  // tslint:disable-next-line: variable-name
  Time: any[];
  Patient: any[];
  Activity: any[];
  Stroke: any[];
  X: any[];
  Y: any[];
  Pressure: any[];
}
