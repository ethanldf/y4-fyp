export class Record {
  // tslint:disable-next-line: variable-name
  _id?: string;
  name: string;
  added: string;
  data: any;
  dementia: any;
}
