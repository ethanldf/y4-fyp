import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';

import { Observable } from 'rxjs';

import { Patient } from './patients';
import { Record } from './records';

const httpOptions = {
  headers: new HttpHeaders({ 'Content-Type': 'application/json' })
};

const httpOptionsDl = {
  headers: new HttpHeaders({
    responseType: 'blob',
    'Content-Type': 'application/json',
  })
};

@Injectable({ providedIn: 'root' })
export class PatientService {
  private patientUrl = 'http://localhost:5000'; // URL to REST API

  constructor(private http: HttpClient) { }

  /** GET patients from the server */
  getPatients(): Observable<Patient[]> {
    return this.http.get<Patient[]>(this.patientUrl + '/patients');
  }

  /** GET patient by id. Will 404 if id not found */
  getPatient(id: string): Observable<any> {
    const url = `${this.patientUrl}/patient/${id}`;
    return this.http.get<Patient>(url);
  }

  /** POST: add a new patient to the server */
  addPatient(patient: Patient): Observable<object> {
    console.log(patient);
    return this.http.post(this.patientUrl + '/add', patient, httpOptions);
  }

  /** PUT: update the patient on the server */
  updatePatient(patient: Patient): Observable<any> {
    return this.http.put(this.patientUrl + '/update', patient, httpOptions);
  }

  /** DELETE: delete the patient from the server */
  deletePatient(patient: Patient, confirm: boolean): Observable<object> {
    if (confirm === true) {
      // tslint:disable-next-line: no-string-literal
      const id = patient._id['$oid'];
      const url = `${this.patientUrl}/delete/${id}`;
      return this.http.delete(url, httpOptions);
    }
  }

  /** PUT: add a new record to patient */
  addRecord(id: string, fd: FormData): Observable<object> {
    const url = `${this.patientUrl}/patient/${id}/add`;
    return this.http.put(url, fd);
  }

  /** PUT: delete the record from the server */
  deleteRecord(pid: string, rid: Record): Observable<object> {
    // tslint:disable-next-line: no-string-literal
    const url = `${this.patientUrl}/patient/${pid}/delete/${rid}`;
    return this.http.put(url, httpOptions);
  }

  /** GET record by id. Will 404 if id not found */
  getRecord(pid: string, rid: string): Observable<any> {
    // tslint:disable-next-line: no-string-literal
    const url = `${this.patientUrl}/patient/${pid}/record/${rid}`;
    return this.http.get<Patient>(url);
  }

  /** GET record analysis by id. Will 404 if id not found */
  getRecordAnal(pid: string, rid: string): Observable<any> {
    // tslint:disable-next-line: no-string-literal
    const url = `${this.patientUrl}/patient/${pid}/record/${rid}/analyse`;
    return this.http.get<Patient>(url);
  }
}
