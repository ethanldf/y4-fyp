import { Record } from './records';

export class Patient {
  // tslint:disable-next-line: variable-name
  _id?: string;
  patientId: string;
  nric: string;
  firstname: string;
  lastname: string;
  gender: string;
  dob: string;
  added: string;
  records: Record[] = [];
}
