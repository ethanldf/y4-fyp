import keras.backend
import keras.layers
import keras.models
import keras.regularizers

import keras_resnet.blocks
import keras_resnet.layers

parameters = {"kernel_initializer": "he_normal"}


def basic_3d(
    filters,
    stage=0,
    block=0,
    kernel_size=3,
    numerical_name=False,
    stride=None,
    freeze_bn=False,
):

    if stride is None:
        if block != 0 or stage == 0:
            stride = 1
        else:
            stride = 2

    if keras.backend.image_data_format() == "channels_last":
        axis = 3
    else:
        axis = 1

    if block > 0 and numerical_name:
        block_char = "b{}".format(block)
    else:
        block_char = chr(ord("a") + block)

    stage_char = str(stage + 2)

    def f(x):
        y = keras.layers.ZeroPadding3D(
            padding=1, name="padding{}{}_branch2a".format(stage_char, block_char)
        )(x)

        y = keras.layers.Conv3D(
            filters,
            kernel_size,
            strides=stride,
            use_bias=False,
            name="res{}{}_branch2a".format(stage_char, block_char),
            **parameters
        )(y)

        y = keras_resnet.layers.BatchNormalization(
            axis=axis,
            epsilon=1e-5,
            freeze=freeze_bn,
            name="bn{}{}_branch2a".format(stage_char, block_char),
        )(y)

        y = keras.layers.Activation(
            "relu", name="res{}{}_branch2a_relu".format(stage_char, block_char)
        )(y)

        y = keras.layers.ZeroPadding3D(
            padding=1, name="padding{}{}_branch2b".format(stage_char, block_char)
        )(y)

        y = keras.layers.Conv3D(
            filters,
            kernel_size,
            use_bias=False,
            name="res{}{}_branch2b".format(stage_char, block_char),
            **parameters
        )(y)

        y = keras_resnet.layers.BatchNormalization(
            axis=axis,
            epsilon=1e-5,
            freeze=freeze_bn,
            name="bn{}{}_branch2b".format(stage_char, block_char),
        )(y)

        if block == 0:
            shortcut = keras.layers.Conv3D(
                filters,
                (1, 1, 1),
                strides=stride,
                use_bias=False,
                name="res{}{}_branch1".format(stage_char, block_char),
                **parameters
            )(x)

            shortcut = keras_resnet.layers.BatchNormalization(
                axis=axis,
                epsilon=1e-5,
                freeze=freeze_bn,
                name="bn{}{}_branch1".format(stage_char, block_char),
            )(shortcut)
        else:
            shortcut = x

        y = keras.layers.Add(name="res{}{}".format(stage_char, block_char))(
            [y, shortcut]
        )

        y = keras.layers.Activation(
            "relu", name="res{}{}_relu".format(stage_char, block_char)
        )(y)

        return y

    return f


def bottleneck_3d(
    filters,
    stage=0,
    block=0,
    kernel_size=3,
    numerical_name=False,
    stride=None,
    freeze_bn=False,
):

    if stride is None:
        if block != 0 or stage == 0:
            stride = 1
        else:
            stride = 2

    if keras.backend.image_data_format() == "channels_last":
        axis = 3
    else:
        axis = 1

    if block > 0 and numerical_name:
        block_char = "b{}".format(block)
    else:
        block_char = chr(ord("a") + block)

    stage_char = str(stage + 2)

    def f(x):
        y = keras.layers.Conv3D(
            filters,
            (1, 1, 1),
            strides=stride,
            use_bias=False,
            name="res{}{}_branch2a".format(stage_char, block_char),
            **parameters
        )(x)

        y = keras_resnet.layers.BatchNormalization(
            axis=axis,
            epsilon=1e-5,
            freeze=freeze_bn,
            name="bn{}{}_branch2a".format(stage_char, block_char),
        )(y)

        y = keras.layers.Activation(
            "relu", name="res{}{}_branch2a_relu".format(stage_char, block_char)
        )(y)

        y = keras.layers.ZeroPadding3D(
            padding=1, name="padding{}{}_branch2b".format(stage_char, block_char)
        )(y)

        y = keras.layers.Conv3D(
            filters,
            kernel_size,
            use_bias=False,
            name="res{}{}_branch2b".format(stage_char, block_char),
            **parameters
        )(y)

        y = keras_resnet.layers.BatchNormalization(
            axis=axis,
            epsilon=1e-5,
            freeze=freeze_bn,
            name="bn{}{}_branch2b".format(stage_char, block_char),
        )(y)

        y = keras.layers.Activation(
            "relu", name="res{}{}_branch2b_relu".format(stage_char, block_char)
        )(y)

        y = keras.layers.Conv3D(
            filters * 4,
            (1, 1, 1),
            use_bias=False,
            name="res{}{}_branch2c".format(stage_char, block_char),
            **parameters
        )(y)

        y = keras_resnet.layers.BatchNormalization(
            axis=axis,
            epsilon=1e-5,
            freeze=freeze_bn,
            name="bn{}{}_branch2c".format(stage_char, block_char),
        )(y)

        if block == 0:
            shortcut = keras.layers.Conv3D(
                filters * 4,
                (1, 1, 1),
                strides=stride,
                use_bias=False,
                name="res{}{}_branch1".format(stage_char, block_char),
                **parameters
            )(x)

            shortcut = keras_resnet.layers.BatchNormalization(
                axis=axis,
                epsilon=1e-5,
                freeze=freeze_bn,
                name="bn{}{}_branch1".format(stage_char, block_char),
            )(shortcut)
        else:
            shortcut = x

        y = keras.layers.Add(name="res{}{}".format(stage_char, block_char))(
            [y, shortcut]
        )

        y = keras.layers.Activation(
            "relu", name="res{}{}_relu".format(stage_char, block_char)
        )(y)

        return y

    return f


class ResNet3D(keras.Model):
    def __init__(
        self,
        inputs,
        blocks,
        block,
        include_top=True,
        classes=2,
        freeze_bn=True,
        numerical_names=None,
        *args,
        **kwargs
    ):
        if keras.backend.image_data_format() == "channels_last":
            axis = 3
        else:
            axis = 1

        if numerical_names is None:
            numerical_names = [True] * len(blocks)

        x = keras.layers.ZeroPadding3D(padding=3, name="padding_conv1")(inputs)
        x = keras.layers.Conv3D(
            64, (7, 7, 7), strides=(2, 2, 2), use_bias=False, name="conv1"
        )(x)
        x = keras_resnet.layers.BatchNormalization(
            axis=axis, epsilon=1e-5, freeze=freeze_bn, name="bn_conv1"
        )(x)
        x = keras.layers.Activation("relu", name="conv1_relu")(x)
        x = keras.layers.MaxPooling3D(
            (3, 3, 3), strides=(2, 2, 2), padding="same", name="pool1"
        )(x)

        features = 64

        outputs = []

        for stage_id, iterations in enumerate(blocks):
            for block_id in range(iterations):
                x = block(
                    features,
                    stage_id,
                    block_id,
                    numerical_name=(block_id > 0 and numerical_names[stage_id]),
                    freeze_bn=freeze_bn,
                )(x)

            features *= 2

            outputs.append(x)

        if include_top:
            assert classes > 0

            x = keras.layers.GlobalAveragePooling3D(name="pool5")(x)
            x = keras.layers.Dense(classes, activation="softmax", name="fc1000")(x)

            super(ResNet3D, self).__init__(inputs=inputs, outputs=x, *args, **kwargs)
        else:
            # Else output each stages features
            super(ResNet3D, self).__init__(
                inputs=inputs, outputs=outputs, *args, **kwargs
            )


class ResNet3D50(ResNet3D):
    def __init__(
        self,
        inputs,
        blocks=None,
        include_top=True,
        classes=2,
        freeze_bn=False,
        *args,
        **kwargs
    ):
        if blocks is None:
            blocks = [3, 4, 6, 3]

        numerical_names = [False, False, False, False]

        super(ResNet3D50, self).__init__(
            inputs,
            blocks,
            numerical_names=numerical_names,
            block=bottleneck_3d,
            include_top=include_top,
            classes=classes,
            freeze_bn=freeze_bn,
            *args,
            **kwargs
        )


class ResNet3D18(ResNet3D):
    def __init__(
        self,
        inputs,
        blocks=None,
        include_top=True,
        classes=2,
        freeze_bn=False,
        *args,
        **kwargs
    ):
        if blocks is None:
            blocks = [2, 2, 2, 2]

        super(ResNet3D18, self).__init__(
            inputs,
            blocks,
            block=basic_3d,
            include_top=include_top,
            classes=classes,
            freeze_bn=freeze_bn,
            *args,
            **kwargs
        )
