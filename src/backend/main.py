from app import app, client, db, fs
from bson.json_util import dumps
from bson.objectid import ObjectId
from flask import Flask
from flask import jsonify, request, make_response
from flask_cors import CORS
from werkzeug import generate_password_hash, check_password_hash, secure_filename
import pandas as pd
import numpy as np
import sys
from io import StringIO
import datetime
from pandas.io.json import json_normalize
import json
import os, keras, csv
import tensorflow as tf
from urllib.parse import unquote
from model import *

app = Flask(__name__)
CORS(app)

pd.options.mode.chained_assignment = None  # default='warn'


@app.route("/add", methods=["POST"])
def add_patient():
    _json = request.json
    _patientId = _json["patientId"]
    _nric = _json["nric"]
    _firstname = _json["firstname"]
    _lastname = _json["lastname"]
    _gender = _json["gender"]
    _dob = _json["dob"]
    _added = _json["added"]
    # validate the received values
    if (
        _patientId
        and _nric
        and _firstname
        and _lastname
        and _gender
        and _dob
        and _added
        and request.method == "POST"
    ):
        if db.patient.find_one({"patientId": _patientId}) != None:
            message = 'Patient with Patient ID: "' + _patientId + '" Already Exists!'
            status_code = 400
            resp = jsonify({"result": message, "status": status_code})
            return resp

        # save details
        id = db.patient.insert(
            {
                "patientId": _patientId,
                "nric": _nric,
                "firstname": _firstname,
                "lastname": _lastname,
                "gender": _gender,
                "dob": _dob,
                "added": _added,
                "records": [],
            }
        )
        message = "Patient added successfully!"
        status_code = 200
        resp = jsonify({"result": message, "status": status_code})
        return resp
    else:
        return not_found()


@app.route("/patients", methods=["GET"])
def patients():
    patients = db.patient.find()
    resp = dumps(patients)
    return resp


@app.route("/patient/<id>", methods=["GET"])
def patient(id):
    patient = db.patient.find_one({"_id": ObjectId(id)})
    resp = dumps(patient)
    return resp


@app.route("/update", methods=["PUT"])
def update_patient():
    _json = request.json
    _id = _json["_id"]
    _patientId = _json["patientId"]
    _nric = _json["nric"]
    _firstname = _json["firstname"]
    _lastname = _json["lastname"]
    _gender = _json["gender"]
    _dob = _json["dob"]
    _added = _json["added"]
    # validate the received values
    if (
        _patientId
        and _nric
        and _firstname
        and _lastname
        and _gender
        and _dob
        and _added
        and request.method == "PUT"
    ):
        # save edits
        db.patient.update_one(
            {"_id": ObjectId(_id["$oid"]) if "$oid" in _id else ObjectId(_id)},
            {
                "$set": {
                    "patientId": _patientId,
                    "nric": _nric,
                    "firstname": _firstname,
                    "lastname": _lastname,
                    "gender": _gender,
                    "dob": _dob,
                    "added": _added,
                }
            },
        )
        message = "Patient updated successfully!"
        status_code = 200
        resp = jsonify({"result": message, "status": status_code})
        return resp
    else:
        return not_found()


@app.route("/delete/<id>", methods=["DELETE"])
def delete_patient(id):
    patient = db.patient.find_one({"_id": ObjectId(id)})

    data = pd.DataFrame.from_dict(patient)
    data_series = pd.Series(data["records"])
    data_dict = data_series.to_dict()
    data_df = pd.DataFrame.from_dict(data_dict)
    data_tran = data_df.transpose()

    if len(data_tran) > 0:
        for i in range(len(data_tran)):
            record_name = data_tran["name"][i]
            record = db.fs.files.find_one({"filename": record_name})
            record_id = record["_id"]
            db.fs.files.delete_one({"_id": record_id})
            db.fs.chunks.delete_many({"files_id": record_id})

    db.patient.delete_one({"_id": ObjectId(id)})
    message = "Patient deleted successfully!"
    status_code = 200
    resp = jsonify({"result": message, "status": status_code})
    return resp


@app.errorhandler(404)
def not_found(error=None):
    message = {"status": 404, "message": "Not Found: " + request.url}
    resp = jsonify(message)
    resp.status_code = 404
    return resp


@app.route("/patient/<id>/add", methods=["PUT"])
def add_record(id):
    if "file" in request.files:
        request_file = request.files["file"]

        if not request_file:
            return "No file"

        if db.fs.files.find({"filename": request_file.filename}).count() > 0:
            message = (
                'Record with Filename: "' + request_file.filename + '" Already Exists!'
            )
            status_code = 400
            resp = jsonify({"result": message, "status": status_code})
            return resp
        else:
            fileId = fs.put(request_file, filename=request_file.filename)
            date_added = str(datetime.datetime.now().isoformat())

            # validate the received values
            if request.method == "PUT":
                # save edits
                db.patient.update_one(
                    {"_id": ObjectId(id)},
                    {
                        "$push": {
                            "records": {
                                "_id": fileId,
                                "name": request_file.filename,
                                "added": date_added,
                            }
                        }
                    },
                )
                message = "Record added successfully!"
                status_code = 200
                resp = jsonify({"result": message, "status": status_code})
                return resp
            else:
                return not_found()


@app.route("/patient/<pid>/delete/<rid>", methods=["PUT"])
def delete_record(pid, rid):
    # validate the received values
    if request.method == "PUT":
        record = db.fs.files.find_one({"_id": ObjectId(rid)})
        if record != None:
            db.fs.files.delete_one({"_id": ObjectId(rid)})
            db.fs.chunks.delete_many({"files_id": ObjectId(rid)})

        # save edits
        db.patient.update_one(
            {"_id": ObjectId(pid)}, {"$pull": {"records": {"_id": ObjectId(rid)}}}
        )

        message = "Record deleted successfully!"
        status_code = 200
        resp = jsonify({"result": message, "status": status_code})
        return resp
    else:
        return not_found()


@app.route("/patient/<pid>/record/<rid>", methods=["GET"])
def record(pid, rid):
    fsFile = fs.find_one({"_id": ObjectId(rid)})

    df_csv = pd.read_csv(fsFile, sep=",", header=None)

    if len(df_csv.columns) == 7:
        df_csv.columns = ["Time", "Patient", "Activity", "Stroke", "X", "Y", "Pressure"]
    else:
        df_csv.columns = ["Time", "Patient", "Stroke", "X", "Y", "Pressure"]

    # clean data
    # if exists rows where pressure == 0, select all rows where pressure == 0
    # split selected rows, if rows are not contiguous
    if df_csv.loc[df_csv["Pressure"] == 0].count().any() > 0:
        zeroList = df_csv.loc[df_csv["Pressure"] == 0].index.values.tolist()

        res, last = [[]], None
        for i in zeroList:
            if last is None or abs(last - i) <= 1:
                res[-1].append(i)
            else:
                res.append([i])
            last = i

        if res[0][0] == 0:
            skip = 1
        else:
            skip = 0

        for x in range(skip, len(res)):
            for y in res[x]:
                df_csv["Time"][y] = df_csv["Time"][y - 1] + 5

        if skip == 1:
            df_csv = df_csv.drop(df_csv.index[: (res[0][-1] + 1)])

    resp = jsonify(
        {
            "Time": df_csv["Time"].values.tolist(),
            "Patient": df_csv["Patient"].values.tolist(),
            "Activity": df_csv["Activity"].values.tolist(),
            "Stroke": df_csv["Stroke"].values.tolist(),
            "X": df_csv["X"].values.tolist(),
            "Y": df_csv["Y"].values.tolist(),
            "Pressure": df_csv["Pressure"].values.tolist(),
        }
    )

    return resp


@app.route("/patient/<pid>/record/<rid>/analyse", methods=["GET"])
def analyse_record(pid, rid):
    # validate the received values
    if request.method == "GET":
        fsFile = fs.find_one({"_id": ObjectId(rid)})

        predicted_class = predict_dem(fsFile)

        # save edits
        db.patient.update_one(
            {"_id": ObjectId(pid), "records._id": ObjectId(rid)},
            {"$set": {"records.$.dementia": int(predicted_class[0])}},
        )

        message = "Analysis successfully completed!"
        status_code = 200
        resp = jsonify({"result": message, "status": status_code})
        return resp
    else:
        return not_found()


def predict_dem(fsFile):
    df_csv = pd.read_csv(fsFile, sep=",", header=None)

    if len(df_csv.columns) == 7:
        df_csv.columns = ["Time", "Patient", "Activity", "Stroke", "X", "Y", "Pressure"]
    else:
        df_csv.columns = ["Time", "Patient", "Stroke", "X", "Y", "Pressure"]

    # create model
    shape, classes = (51, 51, 51, 1), 2
    x = keras.layers.Input(shape)
    blocks = [2, 2, 2, 2]
    block = basic_3d

    loaded_model = ResNet3D18(x, classes=classes)

    # load weights into new model
    loaded_model.load_weights("models/demmodel_res18_100.h5")

    loaded_model._make_predict_function()
    graph = tf.get_default_graph()

    # format data
    formatted_columns = []
    all_columns = []
    col = ["X", "Y", "Stroke"]

    for i in col:
        col_arr = df_csv.iloc[:][i].values

        if len(col_arr) < int(1099959 / 3):
            offset = int(1099959 / 3) - len(col_arr)
            offset_arr = np.zeros(shape=(offset))
            col_arr = np.append(col_arr, offset_arr)

        all_columns = np.append(all_columns, col_arr)

    formatted_columns = np.reshape(all_columns, (1, len(all_columns)))
    X_test = formatted_columns

    # feature-scale for 3d cnn
    min_value = 0
    max_value = 0
    total_range = 29700
    scale = 50

    for i in range(1):
        for j in range(0, int(len(formatted_columns[0]))):
            X_test[i][j] = int(
                round(((X_test[i][j] - min_value) / total_range) * scale)
            )

    df_template = (1, 51, 51, 51)
    df_shape = np.empty(df_template)

    for i in range(1):
        x = 0
        y = int(1 * (len(formatted_columns[0]) / 3))
        z = int(2 * (len(formatted_columns[0]) / 3))
        count = 1

        for j in range(0, int(1 * (len(formatted_columns[0]) / 3))):
            df_x = X_test[i][x]
            df_y = X_test[i][y]
            df_z = X_test[i][z]

            df_shape[i][int(df_x)][int(df_y)][int(df_z)] = count

            x = x + 1
            y = y + 1
            z = z + 1
            count = count + 1

            if count > int(1 * (len(formatted_columns[0]) / 3)):
                count = 1

    # reshape tensor
    df_final = df_shape.reshape(df_shape.shape[0], 51, 51, 51, 1)

    # prediction for dementia assessment
    with graph.as_default():
        prediction = loaded_model.predict(df_final, batch_size=8, verbose=1)

    predicted_class = prediction.argmax(axis=-1)

    return predicted_class


if __name__ == "__main__":
    app.run(host="localhost", threaded=False)
