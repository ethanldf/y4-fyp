import numpy as np
import pandas as pd
import glob

dfff = pd.DataFrame()
counter = 1

label_data = pd.read_excel('20200205.xlsx', header=None)
label_data.columns = ["Patient_id", "Label"]

for x in glob.glob('*.txt'):
    print(x)
    data = pd.read_csv(x, sep=",", header=None)

    if (len(data.columns) == 7):
        data.columns = ["Time", "Patient ID", "Activity ID", "Stroke", "X", "Y", "Pressure"]
    else:
        data.columns = ["Time", "Patient ID", "Stroke", "X", "Y", "Pressure"]

    patient_id = x.split('_')[1][2:5]
    patient_label = label_data.loc[label_data['Patient_id'] == int(patient_id)]['Label']
    data.loc[:,'Label'] = patient_label.values[0]

    for i in data.columns:
        if i in ['X', 'Y', 'Pressure', 'Stroke', 'Label']:
            dff = data.loc[:, i:i]
            dff = dff.add_suffix(counter)
            dff = dff.reset_index(drop=True)

            dfff = pd.concat([dff, dfff], axis=1)

    counter += 1

dfff.to_csv('collection.csv', index=False)
