# Timelog

* Developing Social Intelligence for Neurocognitive Assessment
* Ethan Lim Ding Feng
* 1800887
* Dr. Harry Nguyen Duy Hoang

## Guidance

* This file contains the time log for your project. It will be submitted along with your final dissertation.
* **YOU MUST KEEP THIS UP TO DATE AND UNDER VERSION CONTROL.**
* This timelog should be filled out honestly, regularly (daily) and accurately. It is for *your* benefit.
* Follow the structure provided, grouping time by weeks.  Quantise time to the half hour.


## Week 1
### 23 Sep 2019
* *1 hours* Read the project guidance notes
* *0.5 hour* brief unscheduled meeting with supervisor

### 27 Sep 2019
* *1 hour* Meeting with supervisor
* *0.5 hour* setup Trello board with supervisor

### 28 Sep 2019
* *2 hour* Research into Python


## Week 2
### 30 Sep 2019
* *1 hour* Meeting with supervisor
* *2 hour* Read paper on In-Pressure......
* *1 hour* Modified dissertation template

### 04 Oct 2019
* *2 hour* Read paper on In-Pressure......
* *2 hours* Research into Machine Learning: Tensorflow

### 06 Oct 2019
* *4 hours* Research into Machine Learning (Concepts and Methodology)
* *1 hours* Research into Tensorflow
* *1 hour* Wrote initial draft of literature review for In-Pressure...


## Week 3
### 07 Oct 2019
* *2 hours* Research into Machine Learning (Concepts and Methodology)
* *2 hours* Research into Tensorflow
* *2 hour* Research into Python

### 09 Oct 2019
* *0.5 hour* Created GitLab repository and cloned the cookiecutter for the projects
* *0.5 hour* Updated Timelog

### 10 Oct 2019
* *4 hours* Meeting with supervisor

### 11 Oct 2019
* *2 hours* Wrote initial draft of dissertation introduction

### 12 Oct 2019
* *1 hour* Research into Python
* *3 hours* Research into Numpy

### 13 Oct 2019
* *3 hours* Research into Pandas
* *2 hour* Read paper on Digital......
* *1 hour* Wrote initial draft of literature review for Digital...


## Week 4
### 14 Oct 2019
* *2 hours* Research into AngularJS

### 15 Oct 2019
* *2 hours* Meeting with supervisor

### 18 Oct 2019
* *1 hour* Research into Pytorch
* *1 hour* Research into Matplotlib

### 19 Oct 2019
* *4 hours* Research into Pytorch
* *1 hour* Research into Artificial Neural Networks (ANN)

### 20 Oct 2019
* *4 hours* Research into Artificial Neural Networks (ANN)


## Week 5
### 21 Oct 2019
* *2 hours* Edit dissertation background
* *3 hours* Discussion with project partner

### 24 Oct 2019
* *2 hours* Built wireframe draft

### 25 Oct 2019
* *4 hours* Research into CNN

### 26 Oct 2019
* *3 hours* Edited dissertation introduction


## Week 6
### 27 Oct 2019 
* *4 hours* Wrote and edited dissertation content

### 28 Oct 2019
* *2 hours* Wrote and edited literature review

### 30 Oct 2019 
* *1 hour* Meeting with supervisor, present wireframe design


## Week 7
### 04 Nov 2019
* *2 hours* Meeting with supervisor and first look into sample project data in form of single .txt
* *1 hour* Mobile application git given and researched
* *1 hour* Research and experimentation with data

### 5 Nov 2019
* *2 hours* Use sample code to extract sample data
* *3 hours* Write initial draft of progress report

### 8 Nov 2019
* *4 hours* Reworked literature review dissertation
* *2 hours* Continue draft on progress report 


## Week 8
### 11 Nov 2019
* *3 hours* Modified introduction and added sources for citation

### 12 Nov 2019
* *3 hours* Experimentation with mobile application

### 15 Nov 2019
* *3.5 hour* Setup and research into Angular 8


## Week 9
### 18 Nov 2019
* *3 hours* Attended machine Learning workshop, conducted by supervisor

### 19 Nov 2019
* *3 hours* Wrote and Tested code to create model of sample data

### 22 Nov 2019
* *3 hours* Visit to NUH, first meeting with NUH clincians
* *2 hour* Discussion of tests and processes used for data collection with clinicans, supervisor, and other fyp students utilising the same system/data


## Week 10
### 25 Nov 2019
* *5 hours* Experimented with RNN, with sample dataset as input
* *2 hours* Added Existing Tools section draft to literature review

### 29 Nov 2019
* *4 hours* Edited and wrote dissertation literature review


## Week 11
### 02 Dec 2019
* *4 hours* Built initial application template design in Angular
* *2 hours* Research and experimentation on newly given dataset

### 03 Dec 2019
* *1 hour* Collection of tablet for data collection

### 06 Dec 2019
* *2 hour* Continued developement of application template design


## Week 12
### 09 Dec 2019
* *2 hours* Continued developement of application template design
* *2 hours* Meeting with NUH clinicians

### 12 Dec 2019
* *2 hours* Progress review with supervisor


## Week 13
### 16 Dec 2019
* *4 hours* Wrote and edited dissertation introduction and literature review

### 19 Dec 2019
* *4 hours* Continued developement of application template design
* *0.5 hour* Initial git commit of application template


## Week 14
### 23 Dec 2019
* *1 hours* Meeting with supervisor
* *6 hours* Developed prototype 3D visualisation of data with matplotlib

### 24 Dec 2019
* *4 hours* Continued development of application

## Week 15
### 30 Dec 2019
* *2 hours* Continued development of application
* *4 Hours* Reworked 3D visualisation using Plotly

### 31 Dec 2019
* *1 hour* Continued development of application
* *4 hours* Setup local mongodb for use as database


## Week 16
### 6 Jan 2020
* *2 hours* Reworked database schema
* *3 hours* Implemented python server for database connection with flask-pymongo

### 8 Jan 2020
* *2 hours* Meeting with supervisor
* *3 hours* Implemented angular service to connect to python server


## Week 17
### 13 Jan 2020
* *3 hours* Implemented prototype file upload function
* *2 hours* Reworked python server, and angular service

### 14 Jan 2020
* *2 hours* Continued development on file upload function
* *2 hours* Tested application with large dataset

## Week 18
### 20 Jan 2020
* *2 hours* Meeting with supervisor, discussion regarding machine learning implementation, given advice to utilise 3D CNN
* *2 hours* Research into 3D CNN

### 24 Jan 2020
* *4 hours* Research into 3D CNN
* *2 hours* Continued development on application


## Week 19
### 27 Jan 2020
* *2 hours* Research into 3D CNN
* *2 hours* Tested prototype 3D CNN model
* *4 hours* Implemented prototype data cleaning function
  
### 30 Jan 2020
* *4 hours* Wrote and edited dissertation
* *3 hours* Implemented prototype download function


## Week 20
### 3 Feb 2020
* *2 hours* Continued development on data cleaning function
* *4 hours* Further 3D CNN testing

### 6 Feb 2020
* *4 hours* Further 3D CNN testing
* *6 hours* Added draft Artificial Intelligence and Social Intelligence section in literature review


## Week 21
### 10 Feb 2020
* *4 hours* Further 3D CNN testing
* *1.5 hour* Meeting with supervisor, for discussion to co-author additional paper for publishing, and classification of project dataset

### 12 Feb 2020
* *1 hour* Meeting with supervisor, for discussion of additional HCI paper
* *3 hours* Wrote HCI paper draft

### 13 Feb 2020
* *3 hours* Wrote initial HCI paper draft

### 14 Feb 2020
* *3 hours* Wrote and edited HCI paper

## Week 22
### 17 Feb 2020
* *5 hours* Wrote and edited HCI paper

### 18 Feb 2020
* *3 hours* Wrote and edited HCI paper
  
### 19 Feb 2020
* *1.5 hour* Meeting with supervisor, for discussion of additional HCI paper
* *3 hours* Wrote and edited HCI paper

### 20 Feb 2020
* *4 hours* Wrote and edited HCI paper

### 21 Feb 2020
* *5 hours* Wrote and edited HCI paper

### 22 Feb 2020
* *2 hours* Submitted HCI paper to supervisor


## Week 22
### 24 Feb 2019
* *2 hours* Wrote draft Requirements section of dissertation
* *4 hours* Further 3D CNN testing
* *1 hour* Created prototype analysis function

### 25 Feb 2019
* *1.5 hours* Wrote and edited Requirements section of dissertation
* *4 hours* Further 3D CNN testing

### 26 Feb 2020
* *1 hour* Online meeting with supervisor, project update and consultation
* *2 hours* Continued testing of analysis function

### 27 Feb 2020
* *3 hours* Implemented update profile function
* *2 hours* Continued testing of analysis function

### 28 Feb 2020
* *4 hours* Implemented new data cleaning function

### 01 Mar 2020
* *4 hours* Implemented working analysis function with 3D CNN model


## Week 22
### 03 Mar 2020
* *2 hours* Meeting with supervisor, project application demo

### 04 Mar 2020
* *2 hours* Online meeting with supervisor, for discusion of dissertation

### 05 Mar 2020
* *2 hours* Switched from local mongoDB to mongoDB Atlas
* *4 hours* Modified python server to use pymongo

### 05 Mar 2020
* *6 hours* Wrote draft Design chapter of dissertation

### 07 Mar 2020
* *4 hours* Wrote and edit Design chapter of dissertation
* *4 hours* Test new 3D CNN model

### 08 Mar 2020
* *4 hours* Wrote and edit Design chapter of dissertation
* *3 hours* Further 3D CNN testing


## Week 23
### 09 Mar 2020
* *3 hours* Wrote and edit Design chapter of dissertation
* *3 hours* Further 3D CNN testing
* *2 hour* Modified application to run on simplehttpserver
 
### 10 Mar 2020
* *4 hours* Wrote draft Implementation chapter of dissertation
* *2 hours* Further 3D CNN testing
* *1 hours* Continued development on application

### 11 Mar 2020
* *2 hours* Modified analysis function
* *5 hours* Further 3D CNN testing

### 12 Mar 2020
* *2 hours* Wrote and edited Implementation chapter of dissertation
* *4 hours* Further 3D CNN testing

### 13 Mar 2020
* *1 hour* Wrote and edited Implementation chapter of dissertation
* *2 hours* Further 3D CNN testing
* *2 hours* Continued development on application

### 14 Mar 2020
* *2 hours* Wrote and edited Implementation chapter of dissertation
* *3 hours* Further 3D CNN testing

### 15 Mar 2020
* *2 hours* Wrote and edited Implementation chapter of dissertation
* *3 hours* Further 3D CNN testing
* *2 hours* Continued development on application


## Week 24
### 16 Mar 2020
* *2 hours* Wrote and edited Implementation chapter of dissertation
* *3 hours* Further 3D CNN testing
* *2 hours* Recorded footage for 1 min fyp video

### 18 Mar 2020
* *1 hours* Recorded footage for 1 min fyp video
* *4 hours* Wrote and edited Implementation chapter of dissertation
* *2 hours* Further 3D CNN testing

### 20 Mar 2020
* *4 hours* Wrote and edited Implementation chapter of dissertation
* *2 hours* Further 3D CNN testing
* *4 hours* Edited footage for 1 min fyo video

### 21 Mar 2020
* *2 hours* Edited footage for 1 min fyp video
* *4 hours* Continued development on application


## Week 25
### 23 Mar 2020
* *4 hours* Implemented new ML model

### 24 Mar 2020
* *2 hours* Meeting with supervisor, project progress review
* *2 hours* Further 3D CNN testing
  
### 25 Mar 2020
* *1 hour* Wrote draft Evaluation chapter of dissertation
* *2 hours* Further 3D CNN testing

### 26 Mar 2020
* *2 hours* Wrote draft Evaluation chapter of dissertation
* *2 hours* Continued development on application

## Week 26
### 30 Mar 2020
* *2 hours* Wrote and edited Evaluation chapter of dissertation

### 01 Apr 2020
* *3 hours* Conducted evaluation with NUH clinicians

### 02 Apr 2020
* *3 hours* Wrote and edited Evaluation chapter of dissertation

### 03 Apr 2020
* *4 hours* Wrote draft Conclusion chapter of dissertation

### 03 Apr 2020
* *2 hours* Wrote and edited Conclusion chapter of dissertation
* *2 hours* Wrote draft abstract for dissertation